 //CP3.0 20180927
#include <Dhcp.h>
#include <Dns.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#include <EthernetUdp.h>
#include <SPI.h>
#include <Servo.h> 
#include <TextFinder.h>
#include <EEPROM.h>
//Dirección MAC
byte mac[] = {  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };
//IPAddress ip(192, 168, 1, 100 ); 
//ESTÁ SÓLO CON LA MAC, OBTIENE LA IP SOLO
IPAddress gateway( 192, 168, 1, 1 );
IPAddress subnet( 255, 255, 255, 0 );
EthernetServer server(84);            
int dirx = 12;
int datax;
int diry = 16;
int datay;
int servopos;
int notservopos;

char smtpserver[] = "0.0.0.0"; //agregar IP
int port = 25;

char serverName[] = "checkip.dyndns.org";
char externalIP[17];

String readString; 

Servo myservo;
Servo notservo;

int relayPin1 = 6;
int relayPin2 = 7;
int relstat1;
int relstat2;

EthernetClient client;
TextFinder finder( client);

char c = ' ';
char matchfail = 0;
long starttime = 0;

unsigned long int x=0;
unsigned long OPTime = millis ();
unsigned long maildelay = 900000;


void setup(){

    Serial.begin(9600);
    Ethernet.begin(mac);

    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);
    digitalWrite(relayPin1, LOW);
    digitalWrite(relayPin2, LOW);
    relstat1 = 0;
    relstat2 = 0;
    myservo.attach(8);
    notservo.attach(9);
    server.begin();

    delay(2000);
    Serial.println(Ethernet.localIP());
    Serial.println(F("CP3.0_ALS / 27092018 / send 'e' to test."));


    datax = EEPROM.read(dirx);
    datay = EEPROM.read(diry);
    servopos = datax;
    notservopos = datay;
    myservo.write(servopos);
    notservo.write(notservopos);
    delay(500);
    digitalWrite(relayPin2, HIGH);
    delay(1500);
    digitalWrite(relayPin2, LOW);

}

void loop(){
    datax = EEPROM.read(dirx);
    datay = EEPROM.read(diry);
	//serial debug
    //Serial.println(datax);
    //Serial.println(datay);
    if(datax == 255 && datay == 255){
        datax = 90;
        datay = 90;     
    }
	
	//actualizar y escribir datos
    //servopos = datax;
    //notservopos = datay;

    //EEPROM.write(dirx, servopos);
    //EEPROM.write(diry, notservopos);


    byte inChar;

    inChar = Serial.read();


    if(inChar == 'e'){
        if (client.connect(serverName, 80)){ 
            Serial.println("connected");
            //make HTTP request
            client.println("GET /");
            client.println();
            delay(1000);
        }//end if 
        else { 
            // you didn't get a connection to the server:
            Serial.println("connection failed");
        }//end else


        if (client.available()){ 
            if(finder.find("IP Address: ")){ 
                for (char k = 0; k < 17; k++){
                    c = client.read();
                    if(c != '<'){
                        Serial.println(c);
                        externalIP[k] = c;
                    }
                    else 
                    break;
                }
            }
        }
        client.flush(); 
        client.stop();

        if(sendEmail()) Serial.println(F("Email sent"));
        else Serial.println(F("Email failed"));
    }

    if(millis() - OPTime >= maildelay){
        if (client.connect(serverName, 80)){
            Serial.println("connected");
            //make HTTP request
            client.println("GET /");
            client.println();
            delay(1000);
        }//end if 
        else { // you didn't get a connection to the server:
            Serial.println("connection failed");
        }//end else


        if (client.available()){
            if(finder.find("IP Address: ")){
                for (char k = 0; k < 17; k++){
                    c = client.read();
                    if(c != '<'){
                        Serial.println(c);
                        externalIP[k] = c;
                    } //endif
                    else 
                    break;
                }//end for
            }//end if
        }//end if
        client.flush(); 
        client.stop();

        if(sendEmail()) Serial.println(F("Email sent"));
        else Serial.println(F("Email failed"));

        OPTime = millis();

    }


    EthernetClient client = server.available();
        if (client) {
        while (client.connected()) {
            if (client.available()) {
                char c = client.read();

                //read char by char HTTP request
                if (readString.length() < 100) {

                    //store characters to string 
                    readString += c; 
                    Serial.print(c);
                } 

                //if HTTP request has ended
                if (c == '\n') {
                    Serial.println(readString); //print to serial monitor for debuging 
                    client.println("HTTP/1.1 200 OK");
                    client.println("Content-Type: text/html");
                    client.println();
                    //dataframe gen
                    if(readString.indexOf("data") >0) {  //checks for "data" page
                        x=x+1; //page upload counter
                        client.print("<HTML><HEAD>");
                        //meta-refresh page every 1 seconds if "datastart" page
                        if(readString.indexOf("datastart") >0) client.print("<meta http-equiv='refresh' content='1'>"); 
                        //meta-refresh 0 for fast data
                        if(readString.indexOf("datafast") >0) client.print("<meta http-equiv='refresh' content='0'>"); 
                        client.print("<title>ARDUINO SERVO TEST 1</title></head><BODY><br>");
                        client.print("ACTUALIZACIONES: ");
                        client.print(x); //current refresh count
                        client.print("<br><br>");

                        //output the value of each analog input pin
                        client.print("POSICION SERVO X: ");
                        client.print(myservo.read());

                        client.print("<br>POSICION SERVO Y: ");
                        client.print(notservo.read());

                        client.print("<br><br>PUNTERO LASER: ");
                        if(relstat1 == 1){
                            client.print("ON");
                        }
                        if(relstat1 == 0){
                            client.print("OFF");
                        }
                        
                        client.print("<br>PAN&TILT: ");
                        if(relstat2 == 1){
                            client.print("ON");
                        }
                        if(relstat2 == 0){
                            client.print("OFF");
                        }

                        client.println("<br><br>ESETEL 2018</BODY></HTML>");
                    }
                    //generate main page with iframe
                    else {
                        client.print("<HTML><HEAD><TITLE>ESETEL CONTROL MODULOS ASL</TITLE></HEAD>");
                        client.print("<center><font face='Lucida Console' size='5'>ESETEL<br>CONTROL REMOTO DE MODULOS<br>");
                        client.println("***NO REFRESCAR ESTA PÁGINA***<br>");
                        client.println("***REFRESCAR OCASIONA LA REPETICIÓN DEL ÚLTIMO COMANDO***<br>");
                        client.println("***USAR BOTON 'ACTUALIZAR INFORMACION' DE SER NECESARIO***<br><br>");
                        client.println("PAN: "); 
                        client.println("<br>");
                        client.println("<a href=\"/?sxp1\"\">+1</a>"); 
                        client.println("<a href=\"/?sxp2\"\">+5</a>");
                        client.println("<a href=\"/?sxp3\"\">+10</a>"); 
                        client.println("<a href=\"/?sxp4\"\">+25</a>"); 
                        client.println("<br>");
                        client.println("<a href=\"/?sxm1\"\">-1</a>"); 
                        client.println("<a href=\"/?sxm2\"\">-5</a>");
                        client.println("<a href=\"/?sxm3\"\">-10</a>"); 
                        client.println("<a href=\"/?sxm4\"\">-25</a>");
                        client.println("<br><br>");
                        client.println("TILT: ");
                        client.println("<br>"); 
                        client.println("<a href=\"/?syp1\"\">+1</a>"); 
                        client.println("<a href=\"/?syp2\"\">+5</a>");
                        client.println("<a href=\"/?syp3\"\">+10</a>"); 
                        client.println("<a href=\"/?syp4\"\">+25</a>"); 
                        client.println("<br>");
                        client.println("<a href=\"/?sym1\"\">-1</a>"); 
                        client.println("<a href=\"/?sym2\"\">-5</a>");
                        client.println("<a href=\"/?sym3\"\">-10</a>"); 
                        client.println("<a href=\"/?sym4\"\">-25</a>");
                        client.println("<br><br>");
                        client.println("PUNTERO LASER: ");
                        client.println("<a href=\"/?rel1y\"\">ON</a>");
                        client.println("<a href=\"/?rel1n\"\">OFF</a>");
                        client.println("PAN&TILT: ");
                        client.println("<a href=\"/?rel2y\"\">ON</a>"); 
                        client.println("<a href=\"/?rel2n\"\">OFF</a>");
//ACÁ HAY QUE CAMBIAR LAS IP SI SON  DISTINTAS***
                        client.print("<BR><BR>INFORMACION MODULOS:<BR>");
                        // client.print("&nbsp;&nbsp;<a href='http://192.168.1.100:84/datastart' target='DataBox' title=''yy''>META-REFRESH</a>");
                        client.print("<a href='http://192.168.1.100:84/data' target='DataBox' title=''xx''>ACTUALIZAR INFORMACION</a>");
                        //client.print("&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://192.168.1.100:84/datafast' target='DataBox' title=''zz''>FAST-DATA</a><BR>");
                        client.print("<br><iframe src='http://192.168.1.100:84/data' width='350' height='250' name='DataBox'>");
                        client.print("</iframe><br><br>(C)ESETEL 2018</font></center><BR></HTML>");

//HASTA ACÁ
                    }
                    delay(1);

                    client.stop();

                    //arduino signal pin control
                    if(readString.indexOf("?sxp1") >0) {
                        servopos = myservo.read();
                        myservo.write(servopos + 1);
                        Serial.println(myservo.read());
                        EEPROM.write(dirx, myservo.read());
                        Serial.println(EEPROM.read(dirx));
                    }
                    if(readString.indexOf("?sxp2") >0) {
                        servopos = myservo.read();
                        myservo.write(servopos + 5);
                        Serial.println(myservo.read());
                        EEPROM.write(dirx, myservo.read());
                        Serial.println(EEPROM.read(dirx));
                    }
                    if(readString.indexOf("?sxp3") >0) {
                        servopos = myservo.read();
                        myservo.write(servopos + 10);
                        Serial.println(myservo.read());
                        EEPROM.write(dirx, myservo.read());
                        Serial.println(EEPROM.read(dirx));
                    }
                    if(readString.indexOf("?sxp4") >0) {
                        servopos = myservo.read();
                        myservo.write(servopos + 25);
                        Serial.println(myservo.read());
                        EEPROM.write(dirx, myservo.read());
                        Serial.println(EEPROM.read(dirx));
					}

                    if(readString.indexOf("?sxm1") >0) {
                        servopos = myservo.read();
                        myservo.write(servopos - 1);
                        Serial.println(myservo.read());
                        EEPROM.write(dirx, myservo.read());
                        Serial.println(EEPROM.read(dirx));
                    }
                    if(readString.indexOf("?sxm2") >0) {
                        servopos = myservo.read();
                        myservo.write(servopos - 5);
                        Serial.println(myservo.read());
                        EEPROM.write(dirx, myservo.read());
                        Serial.println(EEPROM.read(dirx));
                    }
                    if(readString.indexOf("?sxm3") >0) {
                        servopos = myservo.read();
                        myservo.write(servopos - 10);
                        Serial.println(myservo.read());
                        EEPROM.write(dirx, myservo.read());
                        Serial.println(EEPROM.read(dirx));
                    }
                    if(readString.indexOf("?sxm4") >0) {
                        servopos = myservo.read();
                        myservo.write(servopos - 25);
                        Serial.println(myservo.read());
                        EEPROM.write(dirx, myservo.read());
                        Serial.println(EEPROM.read(dirx));
                    }

                    if(readString.indexOf("?syp1") >0) {
                        notservopos = notservo.read();
                        notservo.write(notservopos + 1);
                        Serial.println(notservo.read());
                        EEPROM.write(diry, notservo.read());
                        Serial.println(EEPROM.read(diry));
                    }
                    if(readString.indexOf("?syp2") >0) {
                        notservopos = notservo.read();
                        notservo.write(notservopos + 5);
                        Serial.println(notservo.read());
                        EEPROM.write(diry, notservo.read());
                        Serial.println(EEPROM.read(diry));
                    }
                    if(readString.indexOf("?syp3") >0) {
                        notservopos = notservo.read();
                        notservo.write(notservopos + 10);
                        Serial.println(notservo.read());
                        EEPROM.write(diry, notservo.read());
                        Serial.println(EEPROM.read(diry));
                    }
                    if(readString.indexOf("?syp4") >0) {
                        notservopos = notservo.read();
                        notservo.write(notservopos + 25);
                        Serial.println(notservo.read());
                        EEPROM.write(diry, notservo.read());
                        Serial.println(EEPROM.read(diry));
                    }

                    if(readString.indexOf("?sym1") >0) {
                        notservopos = notservo.read();
                        notservo.write(notservopos - 1);
                        Serial.println(notservo.read());
                        EEPROM.write(diry, notservo.read());
                        Serial.println(EEPROM.read(diry));
                    }
                    if(readString.indexOf("?sym2") >0) {
                        notservopos = notservo.read();
                        notservo.write(notservopos - 5);
                        Serial.println(notservo.read());
                        EEPROM.write(diry, notservo.read());
                        Serial.println(EEPROM.read(diry));
                    }
                    if(readString.indexOf("?sym3") >0) {
                        notservopos = notservo.read();
                        notservo.write(notservopos - 10);
                        Serial.println(notservo.read());
                        EEPROM.write(diry, notservo.read());
                        Serial.println(EEPROM.read(diry));
                    }
                    if(readString.indexOf("?sym4") >0) {
                        notservopos = notservo.read();
                        notservo.write(notservopos - 25);
                        Serial.println(notservo.read());
                        EEPROM.write(diry, notservo.read());
                        Serial.println(EEPROM.read(diry));
                    }
                    if(readString.indexOf("?rel1y") >0) {
                        digitalWrite(relayPin1, HIGH);
                        relstat1 = digitalRead(relayPin1);
                        Serial.println(digitalRead(relayPin1));
                        Serial.println(relstat1);
                    }
                    if(readString.indexOf("?rel1n") >0) {
                        digitalWrite(relayPin1, LOW);
                        relstat1 = digitalRead(relayPin1);
                        Serial.println(digitalRead(relayPin1));
                        Serial.println(relstat1);
                    }
                    if(readString.indexOf("?rel2y") >0) {
                        digitalWrite(relayPin2, HIGH);
                        relstat2 = digitalRead(relayPin2);
                        Serial.println(digitalRead(relayPin2));
                        Serial.println(relstat2);
                    }
                    if(readString.indexOf("?rel2n") >0) {
                        digitalWrite(relayPin2, LOW);
                        relstat2 = digitalRead(relayPin2);
                        Serial.println(digitalRead(relayPin2));
                        Serial.println(relstat2);
                    }

                    //clearing string for next read

                    //EEPROM.update(dirx, servopos);
                    //EEPROM.update(diry, notservopos);
                    readString="";

                }
            }
        }
    }
}

byte sendEmail() {
    byte thisByte = 0;
    byte respCode;

    if(client.connect(smtpserver,port) == 1) {
        Serial.println(F("connected"));
    }
    else{
        Serial.println(F("connection failed"));
        return 0;
    }

    if(!eRcv()) return 0;

    Serial.println(F("Sending hello"));
    client.println("EHLO 192.168.0.10");
    if(!eRcv()) return 0;

    Serial.println(F("Sending auth login"));
    client.println("auth login");
    if(!eRcv()) return 0;

    Serial.println(F("Sending User"));
	//REEMPLAZAR CON USUARIO Y CONTRASEÑA
    //base64 encoded user
    client.println(F("000000000000000000000000")); 
    if(!eRcv()) return 0;

    Serial.println(F("Sending Password"));
    //base64 encoded password
    client.println(F("0000000000000000")); 



    if(!eRcv()) return 0;

    //sender email address
    Serial.println(F("Sending From"));
    client.println("MAIL From: MAIL@SERVER.CL");
    if(!eRcv()) return 0;

    //recipient address
    Serial.println(F("Sending To"));
    client.println("RCPT To: MAIL@SERVER.CL");
    if(!eRcv()) return 0;

    Serial.println(F("Sending DATA"));
    client.println("DATA");
    if(!eRcv()) return 0;

    Serial.println(F("Sending email"));

    //recipient address
    client.println("To: ESETEL MAIL@SERVER.CL");

    //sender address
    client.println("From: ARDUINO MAIL@SERVER.CL");

    client.println("Subject: ARDUINO INFO LOG"); 

    client.println("IP LOCAL");
    client.println(Ethernet.localIP());
    client.println("IP PUBLIC");
    client.println(externalIP);
    client.println("END OF MESSAGE");
    client.println(".");

    if(!eRcv()) return 0;

    Serial.println(F("Sending QUIT"));
    client.println("QUIT");
    if(!eRcv()) return 0;

    client.stop();

    Serial.println(F("disconnected"));

    return 1;
    
}

byte eRcv(){
    byte respCode;
    byte thisByte;
    int loopCount = 0;

    while(!client.available()) {
        delay(1);
        loopCount++;

        // if nothing received for 10 seconds, timeout
        if(loopCount > 10000) {
            client.stop();
            Serial.println(F("\r\nTimeout"));
            return 0;
        }
    }

    respCode = client.peek();

    while(client.available()){
        thisByte = client.read(); 
        Serial.write(thisByte);
    }

    if(respCode >= '4'){
        efail();
        return 0; 
    }

    return 1;
}


void efail(){
    byte thisByte = 0;
    int loopCount = 0;

    client.println(F("QUIT"));

    while(!client.available()) {
        delay(1);
        loopCount++;

        // if nothing received for 10 seconds, timeout
        if(loopCount > 10000) {
            client.stop();
            Serial.println(F("\r\nTimeout"));
            return;
        }
    }

    while(client.available()){
        thisByte = client.read(); 
        Serial.write(thisByte);
    }

    client.stop();

    Serial.println(F("disconnected"));
}