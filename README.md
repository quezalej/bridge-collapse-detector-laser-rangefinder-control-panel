# Bridge collapse detector

## About
A bridge in Chile was showing structural damage, but was still standing. A system was assembled to monitor the distance between the two collapsing parts of the bridge, and to alert of any changes. Due to the installation of the device in a high up, hard to access location, it was necessary for this device to be autonomous, reliable and configurable remotely.
The objective was to create a device that could perform the following tasks:
- Connect to the internet on startup
- Forward local and public IP addresses by sending an email for remote access
- A web interface for remote operation
- Allow calibration of the rangefinder direction by controlling servo motors through the web interface
- Periodically read the distance information from a laser rangefinder and detect changes
- Display its current status in the web interface

## Construction
For this project I started with an Arduino UNO, but I quickly reached the 32kb limit of flash memory, so I upgraded to an Arduino Mega with 256kb.
An ethernet shield (HR911105A) was used for networking, and an opto isolated 2 relay module was used for turning on/off a visible laser pointer (for visual calibration) and the servomotors.
The platform on which the rangefinder and laser pointer were mounted was custom made. The servo motors are able to pan and tilt the platform.

## Sketch information
The [program](CP3.0.ino) for the Arduino board takes care of controlling the rotation of the servos, stores the current rotation in EEPROM, and loads it back in case of a loss of power. An HTML interface accessible through a web browser is able to display the status of the Arduino board, and send commands to power a laser pointer and the servomotors, since it is not necessary for them to be powered after the calibration process is finished. Since it was not possible to physically access the device after installation, the program gathers the network information and sends an email with the local and public IP addresses to access the web interface. All of this was possible by using the Arduino Ethernet library (https://github.com/arduino-libraries/Ethernet)

## Additional hardware
The device was additionally fitted with a battery system recharged via solar panels, an interconnected alarm system, and a wireless internet access point to enable networking.

## Pictures
- Screenshot of the web interface
!["Screenshot of the web interface"](IMAGES/menu_screenshot.jpg)

- Testing before full assembly
!["Testing before full assembly"](IMAGES/pic1.jpg)

- Rangefinder platform
!["Rangefinder platform"](IMAGES/pic2.jpg)

- The Arduino Mega+Ethernet shield, relay module and voltage regulators for power
!["The Arduino Mega+Ethernet shield, relay module and voltage regulators for power"](IMAGES/arduino_module.jpg)

- Hardware bay assembled
!["Hardware bay assembled"](IMAGES/hardware.jpg)


